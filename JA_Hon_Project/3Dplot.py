import o32reader as rdr
import adcarray as adc
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d.axes3d import Axes3D

from matplotlib.colors import LightSource
from matplotlib import cm


a=adc.adcarray()

data=np.load('DATA/Cosmics/cosmic_0134_3668.npy')

xmin=100
xmax=120

xdiff=xmax-xmin

ymin=8
ymax=12

ydiff=ymax-ymin

#Create x-y plane

x = np.arange(0,xdiff,1)
y = np.arange(0,ydiff,1)

#Create grid for plane
xs, ys = np.meshgrid(x, y)

#Define z values
#values=np.sum(a.data,axis=2)

tb=int(input('Choose timebin start position: '))
n=int(input('Choose number of timebin: '))


for i in range(tb,tb+n):
	zs = (data[ymin:ymax,xmin:xmax,i])# values


#	plt.subplot(1,tb,i+1)
	#Create plot
	fig = plt.figure()
	ax = Axes3D(fig)
	ax.plot_surface(xs, ys, zs, rstride=1, cstride=1)#, cmap='hot')


	plt.title('Timebin:'+str(i))
	plt.xlabel('Pad column')
	plt.ylabel('Pad row')
	ax.set_zlabel('ADC value')
#	cset = ax.contourf(xs,ys,zs, cmap=cm.coolwarm)




plt.show()
'''


# 3D Plot
fig = plt.figure()
ax3D = fig.add_subplot(111, projection='3d')
p3d = ax3D.scatter(xs, ys, 1, s=30, c=zs, marker='o')                                                                                

plt.show()

'''
