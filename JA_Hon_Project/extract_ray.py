import pylab as pl
import numpy as np
import adcarray as adc
import o32reader as rdr

#Choose file
filename='DATA/0195'

r=rdr.o32reader(filename)



a=adc.adcarray()



#Create list of events
events=[]
e_n=' '

#While there are events left to be extracted
while e_n != '':
	e_n=(input('Choose events number: '))

	#Record event
	if e_n !='':
		events.append(int(e_n))

#Sort events, to put events into ascending order
events.sort()

c=0
count=0

#Cycle through events
for raw in r:
	c+=1

	print(c)

	#Cycle through list
	for f in events:
		if c==f:			#If event in list...
			a.analyse_event(raw)	#Analyse event
			count+=1
			np.save('DATA/Cosmics/cosmic_'+(filename[5:9])+'_'+str(c),a.data)

	#If all cosmics are extracted, terminate code
	if count==len(events):
		break

