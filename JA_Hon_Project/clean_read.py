import pylab as pl
import numpy as np
import adcarray as adc
import o32reader as rdr

filename='DATA/0181'

r=rdr.o32reader(filename)



a=adc.adcarray()

a.clean_data=np.load('DATA/mean_data_'+filename[5:9]+'.npy')

A=10
B=15

def clean_read():

	s=rdr.o32reader(filename)
	c=0
	for raw in s:
		c+=1
	
		print('Analysis')
		print(c)
		if c==2:#>=A and c<=B:
			a.analyse_event(raw)

			for p in range(5,15):
				a.clean_data=(a.data-a.clean_data)
				print('Analysis of ',c)
				pl.subplot(2,2,1)
				pl.title('Data')


				pl.imshow(a.data[:,:,p],aspect='auto')
				pl.colorbar()

				pl.subplot(2,2,2)
				pl.hist(a.data.flatten(),bins=100)


				pl.subplot(2,2,3)
		
				pl.title('Cleaned Data')
				pl.imshow(a.clean_data[:,:,p],aspect='auto')
				pl.colorbar()



				pl.subplot(2,2,4)
				pl.hist(a.clean_data.flatten(),bins=100)

				print(p)
				pl.show()


		if c==10:
			break
clean_read()
