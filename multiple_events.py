# macro to analyze raw data from a single ALICE TRD readout chamber
#
# (c) Raynette van Tonder (2015)
#

from pylab import *
import matplotlib.pyplot as plt
import events

def pad_rms_array(row, pad):
    real_row = 11-row
    mean_rms = get_mean_rms_by_pad(real_row,pad)
    return mean_rms[1]

def get_ADC(robnr,mcmnr,adcnr,timebin):
    return data[robnr,mcmnr,adcnr,timebin]

def get_ADC_by_pad(row,pad,timebin):
    rob = get_rob(row,pad)
    mcm = get_mcm(row,pad)
    channel = which_adc(pad)
    adc = get_ADC(rob,mcm,channel,timebin)
    return adc

def set_ADC(robnr,mcmnr,adcnr,timebin,value):
    data[robnr,mcmnr,adcnr,timebin] = value

def get_all_ADC_by_pad(row,pad):
    adcs = 0.0*zeros(30)
    for i in range(len(adcs)):
        adcs[i] = get_ADC_by_pad(row,pad,i)
    return adcs

def get_rob(real_row, padnr):
    if (real_row in [0,1,2,3]) and (padnr in range(72)):
        robnr = 1
    elif (real_row in [4,5,6,7]) and (padnr in range(72)):
        robnr = 3
    elif (real_row in [8,9,10,11]) and (padnr in range(72)):
        robnr = 5
    elif (real_row in [0,1,2,3]) and (padnr in range(72,144)):
        robnr = 0
    elif (real_row in [4,5,6,7]) and (padnr in range(72,144)):
        robnr = 2
    elif (real_row in [8,9,10,11]) and (padnr in range(72,144)):
        robnr = 4
    return robnr

def get_mcm(real_row, padnr):
    if (real_row in [3,7,11]):
        if (padnr in range(18)) or (padnr in range(72,90)):
            mcm_nr = 15
        elif (padnr in range(18,36)) or (padnr in range(90,108)):
            mcm_nr = 14
        elif (padnr in range(36,54)) or (padnr in range(108,126)):
            mcm_nr = 13
        elif (padnr in range(54,72)) or (padnr in range(126,144)):
            mcm_nr = 12
    if (real_row in [2,6,10]):
        if (padnr in range(18)) or (padnr in range(72,90)):
            mcm_nr = 11
        elif (padnr in range(18,36)) or (padnr in range(90,108)):
            mcm_nr = 10
        elif (padnr in range(36,54)) or (padnr in range(108,126)):
            mcm_nr = 9
        elif (padnr in range(54,72)) or (padnr in range(126,144)):
            mcm_nr = 8
    if (real_row in [1,5,9]):
        if (padnr in range(18)) or (padnr in range(72,90)):
            mcm_nr = 7
        elif (padnr in range(18,36)) or (padnr in range(90,108)):
            mcm_nr = 6
        elif (padnr in range(36,54)) or (padnr in range(108,126)):
            mcm_nr = 5
        elif (padnr in range(54,72)) or (padnr in range(126,144)):
            mcm_nr = 4
    if (real_row in [0,4,8]):
        if (padnr in range(18)) or (padnr in range(72,90)):
            mcm_nr = 3
        elif (padnr in range(18,36)) or (padnr in range(90,108)):
            mcm_nr = 2
        elif (padnr in range(36,54)) or (padnr in range(108,126)):
            mcm_nr = 1
        elif (padnr in range(54,72)) or (padnr in range(126,144)):
            mcm_nr = 0
    return mcm_nr

def which_adc(padnr):
    modpad = padnr%18
    channel = modpad+2
    return channel

def get_mean_rms_by_pad(row,pad):
    adcs = get_all_ADC_by_pad(row,pad)
    N = len(adcs)
    adc_sum = sum(adcs)
    sum_square = sum(adcs**2)
    mean = adc_sum/N
    rms = (sum_square/N - mean**2)**0.5
    return ((mean,rms))

def unbin_data(word,start,stop):
    unbin = int(word[start:stop],2)
    return unbin

def unhex_data(hex_data):
    N = len(hex_data)
    bin_data = []
    scale = 16
    num_of_bits = len(hex_data[0]*4)
    for j in range(N):
        unhex = bin(int(hex_data[j],scale))[2:].zfill(num_of_bits)
        bin_data.append(unhex)
    return bin_data

def get_data(bin_data,hex_data):
    N = len(bin_data)
    countadc = 0
    channel = 0
    timebin = 0
    header_count = 0
    for k in range(N):
        if hex_data[k] == '00000000':
            u = 0
        elif hex_data[k] == "10001000":
            header_count +=1
        elif header_count == 2 and hex_data[k-1] == "10001000":
            u = 0
        elif header_count == 2 and hex_data[k+1][7] == 'c':
            u = 0
            header_count = 0
        elif hex_data[k][7] == 'c':
            rob_pos = unbin_data(bin_data[k],1,4)
            mcm_pos = unbin_data(bin_data[k],4,8)
            channel = 0
            timebin = 0
        else:
            countadc += 1
            adc1 = unbin_data(bin_data[k],0,10)
            timebin += 1
            set_ADC(rob_pos,mcm_pos,channel,timebin-1,adc1)
            adc2 = unbin_data(bin_data[k],10,20)
            timebin += 1
            set_ADC(rob_pos,mcm_pos,channel,timebin-1,adc2)
            adc3 = unbin_data(bin_data[k],20,30)
            timebin += 1
            set_ADC(rob_pos,mcm_pos,channel,timebin-1,adc3)
            if countadc == 10:
                channel += 1
                countadc = 0
                timebin = 0

def read_single_event(File1,File2,event_nr):
    raw_data = events.read_events(File1,File2,event_nr)
    fix = events.strip(raw_data)
    bin_data = unhex_data(fix)
    get_data(bin_data,fix)

def read_multiple_events(File1,File2, event_nrs,data):
    N = len(event_nrs)
    for i in range(N):
        read_single_event(File1,File2,event_nrs[i])
        sum_adc_counts(pad_adc_sum, pad_adc_sum2,Lx,Ly)
        data = clear_data_array(data)
    mean, rms = get_mean_rms_multiple_events(pad_adc_sum,pad_adc_sum2,N)
    return mean, rms


def sum_adc_counts(pad_adc_sum,pas_adc_sum2, Lx, Ly):
    for i in range(Ly):
        for j in range(Lx):
            real_row = 11 - i
            adcs = get_all_ADC_by_pad(real_row,j)
            adc_sum = sum(adcs)
            adc_sum2 = sum(adcs**2)
            pad_adc_sum[i,j] = pad_adc_sum[i,j] + adc_sum
            pad_adc_sum2[i,j] = pad_adc_sum2[i,j] + adc_sum2
    return pad_adc_sum, pad_adc_sum2

def get_mean_rms_multiple_events(pad_adc_sum,pad_adc_sum2,Nr_of_events):
    total_adc = Nr_of_events*30.
    mean = pad_adc_sum/total_adc
    rms = (pad_adc_sum2/total_adc - mean**2)**0.5
    return mean, rms

def clear_data_array(data):
    clear = array([[[[0]*30]*21]*16]*6,float)
    data = clear
    return data

############################################################################
#Read a single event

data = array([[[[0]*30]*21]*16]*6,float)
read_single_event("/home/trd/chris/dim/data/0089",
                  "/home/trd/chris/dim/data/0089",
                  1)

Lx = 144
Ly = 12

pad_adc_sum = array([[0]*Lx]*Ly,float)
pad_adc_sum2 = array([[0]*Lx]*Ly,float)

pad_rms = array([[0]*Lx]*Ly,float)

for i in range(Ly):
    for j in range(Lx):
        #rownr = i
        #padnr = j
        padrms = pad_rms_array(i,j)
        pad_rms[i,j] = padrms

#the_events = linspace(1,70,70)

#mean,rms = read_multiple_events("run_0051.txt","run_0053.txt",the_events,data)

#imshow(rms, interpolation = 'nearest',origin='lower',aspect = 'auto')
##grid(True)
#colorbar()
#show()

imshow(pad_rms, interpolation = 'nearest',origin='lower',aspect = 'auto')
##grid(True)
colorbar()
show()

#print get_mean_rms_by_pad(3,78)
#print pad_rms[8,79]

#for i in range(0,3):
#    for j in range (70,144):
#        print i,j, get_all_ADC_by_pad(i,j)
                                 
